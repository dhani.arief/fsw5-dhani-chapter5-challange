const express = require('express');
const app = express();
const port = 8888;

app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));


app.get('/',(req,res)=>{
    res.render('index');
});

app.get('/trial-game',(req,res)=>{
    res.render('game');
});

app.listen(port,()=>{
    console.log(`this app is running in port ${port}`);
});